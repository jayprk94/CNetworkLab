#include "user.h"

// Linkedlist Operations and wrappers
int indexOfUser(List_t* list, char* username, User** user){
    int index = 0;
    node_t* current = list->head;
    while (current != NULL){
        if (strcmp(((User*)(current->value))->username, username) == 0){
            // matching found, return index
            *user = (User*)(current->value);
            // if(mutex != NULL){pthread_mutex_unlock(mutex);}
            return index;
        }
        current = current->next;
        ++index;
    }
    // if(mutex != NULL){pthread_mutex_unlock(mutex);}
    *user = NULL;
    return -1;
}

void insertUser(List_t* list, User* user, pthread_mutex_t* mutex){
    // if(mutex != NULL){pthread_mutex_lock(mutex);}
    insertFront(list, (void*)user);
    // if(mutex != NULL){pthread_mutex_unlock(mutex);}
}

void removeUser(List_t* list, char* username){
    User*  user;
    // if(mutex != NULL){pthread_mutex_lock(mutex);}
    int index = indexOfUser(list, username, &user);
    if (index > -1)
        removeByIndex(list, index);
    // if(mutex != NULL){pthread_mutex_unlock(mutex);}
}

void usersToString(char** outstring, List_t* list, pthread_mutex_t* mutex, char *doNotInclude){
    // if(mutex != NULL){pthread_mutex_lock(mutex);}

    char buffer[1024] = "";

    node_t* current = list->head;
    while(current != NULL){
        User* current_user = (User*)(current->value);
        if (strcmp(current_user->username, doNotInclude) != 0) {
            strcat(buffer, current_user->username);
            strcat(buffer, "\n");
        }
        current = current->next;
    }

    *outstring = malloc(sizeof(char) * (strlen(buffer) + 1)); 
    strcpy(*outstring, buffer);
    *(*outstring + strlen(buffer)) = 0;

    // if(mutex != NULL){pthread_mutex_unlock(mutex);}
}
