#include "sbuf.h"

void sbuf_init(sbuf_t *sp, int n)
{
    sp->buf = calloc(n, sizeof(Job));
    sp->n = n; /* Buffer holds max of n items */
    sp->front = sp->rear = 0; /* Empty buffer iff front == rear */
    sem_init(&sp->mutex, 0, 1); /* Binary semaphore for locking */
    sem_init(&sp->slots, 0, n); /* Initially, buf has n empty slots */
    sem_init(&sp->items, 0, 0); /* Initially, buf has 0 items */
}

/* Clean up buffer sp */
void sbuf_deinit(sbuf_t *sp)
{
    free(sp->buf);
}

/* Insert item onto the rear of shared buffer sp */
void sbuf_insert(sbuf_t *sp, User* user, int type, char* message)
{
    sem_wait(&sp->slots); /* Wait for available slot */
    sem_wait(&sp->mutex); /* Lock the buffer */
    sp->buf[(++sp->rear)%(sp->n)].user = user; /* Insert the item */
    sp->buf[(++sp->rear)%(sp->n)].type = type; /* Insert the item */
    sp->buf[(++sp->rear)%(sp->n)].message = message; /* Insert the item */
    sem_post(&sp->mutex); /* Unlock the buffer */
    sem_post(&sp->items); /* Announce available item */
}

/* Remove and return the first item from buffer sp */
void sbuf_remove(sbuf_t *sp, User** user, int* type, char** message)
{
    int item;
    sem_wait(&sp->items); /* Wait for available item */
    sem_wait(&sp->mutex); /* Lock the buffer */
    *user = sp->buf[(++sp->front)%(sp->n)].user; /* Remove the item */
    *type = sp->buf[(++sp->front)%(sp->n)].type; /* Remove the item */
    *message = sp->buf[(++sp->front)%(sp->n)].message; /* Remove the item */
    sem_post(&sp->mutex); /* Unlock the buffer */
    sem_post(&sp->slots); /* Announce available slot */
}