#include "helpers.h"

void print_header(FILE* outfile, petr_header* header){
	fprintf(outfile, "\n>> Header information\n>> length : %lu\n>> type   : 0x%x\n\n", (unsigned long)(header->msg_len), header->msg_type);
}

void write_buffer(char* buffer, char* body){
	// need to check for buffer size
	petr_header* buf_header = (petr_header*)(buffer);
	strcpy((char*)(buf_header + 1), body);
}