#include "service.h"
int logout(List_t* users, List_t* rooms, User* user, pthread_mutex_t *rooms_mutex, pthread_mutex_t *users_mutex){
    petr_header* res_header = malloc(sizeof(res_header));
    
    // pthread_mutex_lock(rooms_mutex);
    node_t* current = rooms->head;
    int index = 0;
    while(current != NULL){
        Room* current_room = (Room*)(current->value);
        if (current_room->creator == user){
            rmdelete(rooms, user, current_room->name, NULL);
        } else {
            removeUser(&current_room->users, user->username);
        }
        current = current->next;
    }
    // pthread_mutex_unlock(rooms_mutex);

    removeUser(users, user->username);

    res_header->msg_type = OK;
    res_header->msg_len = 0;
    wr_msg(user->socket_fd, res_header, "");

    free(user);
    return 0;
}
/*
int logout(List_t* users, List_t* rooms, User* user, pthread_mutex_t *rooms_mutex, pthread_mutex_t *users_mutex){
    petr_header* res_header = malloc(sizeof(res_header));
    // TODO : remove user from the users?
    // remove chat rooms created by user
    // pthread_mutex_lock(rooms_mutex);
    node_t *rm_ptr = rooms->head;
    res_header->msg_type = RMCLOSED;
    int index = 0;
    while(rm_ptr != NULL) {
        Room *cur_room = (Room *)(rm_ptr->value);
        if (strcmp(((User *)(cur_room->creator))->username, user->username) == 0) {
            node_t *room_user = cur_room->users.head;
            res_header->msg_len = strlen(cur_room->name) + 1;
            while (room_user != NULL) {
                User *cur_user = (User *)(room_user->value);
                if (strcmp(cur_user->username, user->username) != 0) {
                    char buf[200];
                    strcpy(buf, cur_room->name);
                    wr_msg(cur_user->socket_fd, res_header, buf);
                }
                node_t *tmp_user = room_user;
                room_user = room_user->next;
                free(tmp_user);
            }

            rm_ptr = rm_ptr->next;
            free(cur_room->name);
            removeByIndex(rooms, index);
            continue;

        } else {
            User **placeholder = NULL;
            if (indexOfUser(&(cur_room->users), user->username, placeholder) > -1) {
                removeUser(&(cur_room->users), user->username);
            }
        }
        rm_ptr = rm_ptr->next;
        index++;
    }    
    // pthread_mutex_unlock(rooms_mutex);

    removeUser(users, user->username);

    res_header->msg_len = 0;
    res_header->msg_type = OK;
    wr_msg(user->socket_fd, res_header, "");
    free(res_header);
    return 0;
}
*/

int rmcreate(List_t* rooms, User* user, char* roomName, pthread_mutex_t* rooms_mutex){
    petr_header* res_header = malloc(sizeof(petr_header));

    Room* room_found;
    if (indexOfRoom(rooms, roomName, rooms_mutex, &room_found) != -1){
        res_header->msg_type = ERMEXISTS;
        res_header->msg_len = 0;
        wr_msg(user->socket_fd, res_header, "");

        free(res_header);
        return 1;
    }

    Room* new_room = malloc(sizeof(Room));
    new_room->creator = user;
    new_room->name = malloc(sizeof(char) * (strlen(roomName) + 1));
    strcpy(new_room->name, roomName);
    insertFront(&(new_room->users), (void*)user);

    insertRoom(rooms, new_room, rooms_mutex);

    res_header->msg_type = OK;
    res_header->msg_len = 0;
    wr_msg(user->socket_fd, res_header, ""); 

    free(res_header); 
    return 0;
}

int rmdelete(List_t* rooms, User* user, char* roomName, pthread_mutex_t* rooms_mutex){
    petr_header* res_header = malloc(sizeof(petr_header));

    Room* room;
    if (indexOfRoom(rooms, roomName, rooms_mutex, &room) == -1){
        // room does not exist
        res_header->msg_type = ERMNOTFOUND;
        res_header->msg_len = 0;
        wr_msg(user->socket_fd, res_header, "");

        free(res_header);
        return 1;
    }

    if (room->creator != user){
        // user doesn't have permission to delete the room
        res_header->msg_type = ERMDENIED;
        res_header->msg_len = 0;
        wr_msg(user->socket_fd, res_header, "");

        free(res_header);
        return 1;
    }

    // Send all users in the room RMCLOSED message
    node_t* cursor = room->users.head;
    while (cursor != NULL){
        if (strcmp(((User *)(cursor->value))->username, user->username) != 0) {
            res_header->msg_type = RMCLOSED;
            res_header->msg_len = strlen(roomName) + 1;
            wr_msg(((User *)(cursor->value))->socket_fd, res_header, roomName);
        }
        cursor = cursor->next;
    }

    removeRoom(rooms, roomName, rooms_mutex);

    res_header->msg_type = OK;
    res_header->msg_len = 0;
    wr_msg(user->socket_fd, res_header, "");

    free(res_header); 
    return 0;
}

void rmlist(List_t* rooms, User* user, pthread_mutex_t* rooms_mutex){
    petr_header* res_header = malloc(sizeof(petr_header));

    char* string;
    roomsToString(&string, rooms, rooms_mutex);

    res_header->msg_type = RMLIST;
    res_header->msg_len = strlen(string) == 0 ? 0 : strlen(string) + 1;

    wr_msg(user->socket_fd, res_header, string);

    free(string);
    free(res_header); 
}

int rmjoin(List_t* rooms, User* user, char* roomName, pthread_mutex_t* rooms_mutex){
    petr_header* res_header = malloc(sizeof(petr_header));

    Room* room_found;
    if (indexOfRoom(rooms, roomName, rooms_mutex, &room_found) == -1){
        res_header->msg_type = ERMNOTFOUND;
        res_header->msg_len = 0;
        wr_msg(user->socket_fd, res_header, "");

        free(res_header);
        return 1;
    }

    // pthread_mutex_lock(rooms_mutex);
    // prevent adding user twice to a room
    User* user_found;
    if (indexOfUser(&(room_found->users), user->username, &user_found) < 0) {
        insertFront(&(room_found->users), (void*)user);
    }
    // pthread_mutex_unlock(rooms_mutex);
    
    res_header->msg_type = OK;
    res_header->msg_len = 0;
    wr_msg(user->socket_fd, res_header, ""); 

    free(res_header);
    return 0;
}

int rmleave(List_t* rooms, User* user, char* roomName, pthread_mutex_t* rooms_mutex){
    petr_header* res_header = malloc(sizeof(petr_header));

    Room* room_found;
    if (indexOfRoom(rooms, roomName, rooms_mutex, &room_found) == -1){
        res_header->msg_type = ERMNOTFOUND;
        res_header->msg_len = 0;
        wr_msg(user->socket_fd, res_header, "");

        free(res_header);
        return 1;
    }

    if (strcmp(room_found->creator->username, user->username) == 0) {
        res_header->msg_type = ERMDENIED;
        res_header->msg_len = 0;
        wr_msg(user->socket_fd, res_header, "");
        free(res_header);
        return 1;
    }

    // pthread_mutex_lock(rooms_mutex);
    User* user_found;
    if (indexOfUser(&(room_found->users), user->username,  &user_found) > -1) {
        removeUser(&(room_found->users), user->username);
    }
    // pthread_mutex_unlock(rooms_mutex);

    res_header->msg_type = OK;
    res_header->msg_len = 0;
    wr_msg(user->socket_fd, res_header, ""); 

    free(res_header);
    return 0;
}

int rmsend(List_t* rooms, User* user, char* string_chunk, pthread_mutex_t* rooms_mutex){
    petr_header* res_header = malloc(sizeof(petr_header));

    char* roomname; char* message;
    parse_string_chunk(string_chunk, &roomname, &message);

    Room* room;
    if (indexOfRoom(rooms, roomname, rooms_mutex, &room) == -1){
        res_header->msg_type = ERMNOTFOUND;
        res_header->msg_len = 0;
        wr_msg(user->socket_fd, res_header, "");
        free(res_header);
        return 1;
    }

    // construct message
    char msg_buffer[2048] = "";
    strcat(msg_buffer, roomname);
    strcat(msg_buffer, "\r\n");
    strcat(msg_buffer, user->username);
    strcat(msg_buffer, "\r\n");
    strcat(msg_buffer, message);

    char* msg_to_send = malloc(sizeof(char) * (strlen(msg_buffer) + 1));
    strcpy(msg_to_send, msg_buffer);
    *(msg_to_send + strlen(msg_buffer)) = 0;

    // send message to all the users in the room // 
    // TODO: check if the sender itself needs to receive the message
    res_header->msg_type = RMRECV;
    res_header->msg_len = strlen(msg_buffer) + 1;

    node_t* current = room->users.head;
    while (current != NULL){
        User* current_user = (User*)(current->value);
        if (strcmp(current_user->username, user->username) != 0)
            wr_msg(current_user->socket_fd, res_header, msg_to_send);

        current = current->next;
    }
    free(msg_to_send);

    res_header->msg_type = OK;
    res_header->msg_len = 0;
    wr_msg(user->socket_fd, res_header, ""); 
    free(res_header);
    return 0;
}

int usrsend(List_t* users, User* user, char* string_chunk, pthread_mutex_t* users_mutex){
    petr_header* res_header = malloc(sizeof(petr_header));

    char* to_username; char* message;
    parse_string_chunk(string_chunk, &to_username, &message);

    //check if user exists
    User* to_user;
    if (indexOfUser(users, to_username, &to_user) == -1){
        // user does not exist
        res_header->msg_type = EUSRNOTFOUND;
        res_header->msg_len = 0;
        wr_msg(user->socket_fd, res_header, "");

        free(res_header);
        return 1;
    }

    // send message to to_user
    char msg_buffer[2048] = "";

    strcat(msg_buffer, user->username);
    strcat(msg_buffer, "\r\n");
    strcat(msg_buffer, message);

    char* message_to_send = malloc(sizeof(char) * (strlen(msg_buffer) + 1));

    strcpy(message_to_send, msg_buffer);
    *(message_to_send + strlen(msg_buffer)) = 0;

    res_header->msg_type = USRRECV;
    res_header->msg_len = strlen(msg_buffer) + 1;
    wr_msg(to_user->socket_fd, res_header, message_to_send);
    free(message_to_send);

    res_header->msg_type = OK;
    res_header->msg_len = 0;
    wr_msg(user->socket_fd, res_header, "");

    free(to_username);
    free(message);
    free(res_header);
    return 0;
}

int usrlist(List_t* users, User* user, pthread_mutex_t* users_mutex){
    petr_header* res_header = malloc(sizeof(petr_header));

    char* msg_buf = NULL;
    usersToString(&msg_buf, users, users_mutex, user->username);

    res_header->msg_type = USRLIST;


    if (strlen(msg_buf) == 0){
        res_header->msg_len = 0;
    } else {
        res_header->msg_len = strlen(msg_buf) + 1;
    }
    wr_msg(user->socket_fd, res_header, msg_buf);

    free(msg_buf);
    free(res_header);
    return 0;
}

void parse_string_chunk(char* string_chunk, char** arg1, char** arg2){
    char *saveptr;
    char *a1, *a2;
    a1 = strtok_r(string_chunk, "\r", &saveptr);
    a2 = strtok_r(NULL, "\r", &saveptr) + 1;
    *arg1 = malloc(sizeof(char) * strlen(a1));
    strcpy(*arg1, a1);
    *arg2 = malloc(sizeof(char) * strlen(a2));
    strcpy(*arg2, a2);
}
