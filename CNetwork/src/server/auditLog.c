#include "auditLog.h"

int writeToAuditLog(FILE *fp, char *msg, pthread_mutex_t* mutex) {
    pthread_mutex_lock(mutex);
    time_t now;
    time(&now);
    char buf[25];
    ctime_r(&now, buf);
    int r = fprintf(fp, "%s\t\t%s\n", buf, msg);
    pthread_mutex_unlock(mutex);
    return r;
}