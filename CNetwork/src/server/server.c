#include "server.h"
#include "protocol.h"
#include <pthread.h>
#include <signal.h>

const char exit_str[] = "exit";

char buffer[BUFFER_SIZE];
pthread_mutex_t buffer_lock = PTHREAD_MUTEX_INITIALIZER;

int total_num_msg = 0;
int listen_fd;

enum msg_types msgTypes;

// custom data structure stuff
// User *allUsers = NULL;
List_t users = {NULL, 0, NULL};
pthread_mutex_t users_lock = PTHREAD_MUTEX_INITIALIZER;

List_t rooms = {NULL, 0, NULL};
pthread_mutex_t rooms_lock = PTHREAD_MUTEX_INITIALIZER;

sbuf_t job_buffer;

FILE *audit_fp;
pthread_mutex_t audit_lock = PTHREAD_MUTEX_INITIALIZER;

List_t client_tids;

void sigint_handler(int sig) {
    printf("shutting down server\n");
    close(listen_fd);
    fclose(audit_fp);
    exit(0);
}

int server_init(int server_port) {
    int sockfd;
    struct sockaddr_in servaddr;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("socket creation failed...\n");
        exit(EXIT_FAILURE);
    } else
        printf("Socket successfully created\n");

    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(server_port);

    int opt = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, (char *)&opt, sizeof(opt)) < 0) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA *)&servaddr, sizeof(servaddr))) != 0) {
        printf("socket bind failed\n");
        exit(EXIT_FAILURE);
    } else
        printf("Socket successfully binded\n");

    // Now server is ready to listen and verification
    if ((listen(sockfd, 1)) != 0) {
        printf("Listen failed\n");
        exit(EXIT_FAILURE);
    } else
        printf("Server listening on port: %d.. Waiting for connection\n", server_port);

    return sockfd;
}

//Function running in job thread
void *process_job() {
    printf("job thread started with tid: %ld\n", pthread_self());
    while (1){
        // Consume job
        User* user; 
        int type; 
        char* message;
        sbuf_remove(&job_buffer, &user, &type, &message);

        char audit_msg[200];

        // TODO: process job
        printf("type is 0x%x, message = %s\n", type, message);
        sprintf(audit_msg, "Job removed:\tType: \"0x%x\"\tUser: \"%s\"\n", type, user->username);
        writeToAuditLog(audit_fp, audit_msg, &audit_lock);
        bzero(audit_msg, 200);
        pthread_mutex_lock(&users_lock);
        pthread_mutex_lock(&rooms_lock);
        switch(type){
            case RMCREATE:
                if (rmcreate(&rooms, user, message, &rooms_lock) == 0) {
                    sprintf(audit_msg, "User \"%s\" has created room \"%s\"", user->username, message);
                }
                break;
            case RMDELETE:
                if (rmdelete(&rooms, user, message, &rooms_lock) == 0) {
                    sprintf(audit_msg, "User \"%s\" has deleted room \"%s\"", user->username, message);
                }
                break;
            case RMLIST:
                rmlist(&rooms, user, &rooms_lock);
                sprintf(audit_msg, "User \"%s\" has copied room list", user->username);
                break;
            case RMJOIN:
                if (rmjoin(&rooms, user, message, &rooms_lock) == 0) {
                    sprintf(audit_msg, "User \"%s\" has joined room \"%s\"", user->username, message);
                }
                break;
            case RMLEAVE:
                if (rmleave(&rooms, user, message, &rooms_lock) == 0) {
                    sprintf(audit_msg, "User \"%s\" has left room \"%s\"", user->username, message);
                }
                break;
            case RMSEND:
                if(rmsend(&rooms, user, message, &rooms_lock) == 0) {
                    sprintf(audit_msg, "User \"%s\" has sent message to room, \"%s\"", user->username, message);
                }
                break;
            case USRSEND:
                if(usrsend(&users, user, message, &users_lock) == 0) {
                    sprintf(audit_msg, "User \"%s\" has sent message to user, \"%s\"", user->username, message);
                }
                break;
            case USRLIST:
                usrlist(&users, user, &users_lock);
                sprintf(audit_msg, "User \"%s\" has copied user list", user->username);
                break;
        }

        if (strlen(audit_msg) > 0) {
            writeToAuditLog(audit_fp, audit_msg, &audit_lock);
        }
        pthread_mutex_unlock(&users_lock);
        pthread_mutex_unlock(&rooms_lock);

        free(message);
    }
}

//Function running in client thread
void *process_client(void* user_ptr) {
    User* user = (User*) user_ptr;
    int client_fd = user->socket_fd;
    //int client_fd = *(int *)clientfd_ptr;
    //free(clientfd_ptr);
    int received_size;
    fd_set read_fds;

    int retval;

    printf("client thread started with tid: %ld\n", pthread_self());

    while (1) {
        FD_ZERO(&read_fds);
        FD_SET(client_fd, &read_fds);
        retval = select(client_fd + 1, &read_fds, NULL, NULL, NULL);
        if (retval != 1 && !FD_ISSET(client_fd, &read_fds)) {
            printf("Error with select() function\n");
            break;
        }

        pthread_mutex_lock(&buffer_lock);

        bzero(buffer, BUFFER_SIZE);

        // check if client has disconnected
        if (recv(client_fd, buffer, sizeof(buffer), MSG_PEEK | MSG_DONTWAIT) == 0) {
            printf("user has disconnected...\n");
        pthread_mutex_lock(&users_lock);
        pthread_mutex_lock(&rooms_lock);
            logout(&users, &rooms, user, &rooms_lock, &users_lock);
        pthread_mutex_unlock(&users_lock);
        pthread_mutex_unlock(&rooms_lock);
        }

        received_size = read(client_fd, buffer, sizeof(buffer));


        if (received_size < 0) {
            printf("Receiving failed\n");
            break;
        } else if (received_size == 0) {
            continue;
        }


        if (strncmp(exit_str, buffer, sizeof(exit_str)) == 0) {
            printf("Client exit\n");
            break;
        } 

        
        total_num_msg++;
        // print buffer which contains the client contents
        printf("Receive message from client: %s\n", buffer);
        printf("Total number of received messages: %d\n", total_num_msg);

        // <<<<
        // make a copy of header information from buffer
        petr_header* buf_header = (petr_header*)buffer;

        printf("received : \n");
        print_header(stdout, buf_header);

        char* message = malloc(sizeof(char) * buf_header->msg_len);
        strncpy(message, (char*)(buf_header + 1), buf_header->msg_len);


        if (buf_header->msg_type == LOGOUT){
            // Logout user
        pthread_mutex_lock(&users_lock);
        pthread_mutex_lock(&rooms_lock);
            logout(&users, &rooms, user, &rooms_lock, &users_lock);
        pthread_mutex_unlock(&users_lock);
        pthread_mutex_unlock(&rooms_lock);
            break;
        }

        // Produce job
        sbuf_insert(&job_buffer, user, buf_header->msg_type, message);
        char audit_msg[200];
        sprintf(audit_msg, "Job added:\tType: \"0x%x\"\tUser: \"%s\"\n", buf_header->msg_type, user->username);
        writeToAuditLog(audit_fp, audit_msg, &audit_lock);

        pthread_mutex_unlock(&buffer_lock);
    }
    pthread_mutex_unlock(&buffer_lock);
    // Close the socket at the end
    printf("Close current client connection\n");
    close(client_fd);
    // TODO: remove user from user list
    return NULL;
}

void run_server(int server_port) {
    listen_fd = server_init(server_port); // Initiate server and start listening on specified port
    int client_fd;
    struct sockaddr_in client_addr;
    int client_addr_len = sizeof(client_addr);

    // pthread_t tid;




    while (1) {
        // Wait and Accept the connection from client
        printf("Wait for new client connection\n");
        int *client_fd = malloc(sizeof(int));
        *client_fd = accept(listen_fd, (SA *)&client_addr, (socklen_t*)&client_addr_len);
        if (*client_fd < 0) {
            printf("server acccept failed\n");
            exit(EXIT_FAILURE);
        } else {
            printf("Client connetion accepted\n");
            // verify user name // reject & close connection if user invalid

            petr_header* msg_header = malloc(sizeof(msg_header));
            if (rd_msgheader(*client_fd, msg_header) != 0){
                // Failed to read head
                ;
            }

            // Login
            if (msg_header->msg_type == LOGIN){
        pthread_mutex_lock(&users_lock);
        pthread_mutex_lock(&rooms_lock);

                // Allocate a user
                User* user = malloc(sizeof(User));
                user->socket_fd = *client_fd;
                user->username = malloc(sizeof(char) * msg_header->msg_len);
                // Get username from message body
                read(*client_fd, user->username, msg_header->msg_len);

                // Check if username already exists
                User* user_found;
                if (indexOfUser(&users, user->username, &user_found) != -1){
                    // Username exists, send error to client
                    petr_header* res_header = malloc(sizeof(petr_header));
                    res_header->msg_len = 0;
                    res_header->msg_type = EUSREXISTS;
                    wr_msg(*client_fd, res_header, NULL);

                    free(user->username);
                    free(user);
                    free(res_header);
        pthread_mutex_unlock(&users_lock);
        pthread_mutex_unlock(&rooms_lock);
                } else {
                    // Username doesn not exist
                    // Put user information to the users list
                    insertUser(&users, user, &users_lock);

                    // Send client that login was successful
                    petr_header* res_header = malloc(sizeof(petr_header));
                    res_header->msg_len = 0;
                    res_header->msg_type = OK;
                    wr_msg(*client_fd, res_header, NULL);
                    char usernamebuf[200];
                    snprintf(usernamebuf, 200, "New user \"%s\" logged in.", user->username);
                    writeToAuditLog(audit_fp, usernamebuf, &audit_lock); 

        pthread_mutex_unlock(&users_lock);
        pthread_mutex_unlock(&rooms_lock);
                    // Spawn client thread
                    pthread_t* tid = malloc(sizeof(pthread_t));
                    insertFront(&client_tids, (void*)tid);
                    pthread_create(tid, NULL, process_client, (void *)user);

                    free(res_header);
                }
            }
        }
    }
    // TODO: free tid list

    bzero(buffer, BUFFER_SIZE);
    close(listen_fd);
    return;
}

int main(int argc, char *argv[]) {
    int opt;
	int jobs = 2;
    char h_flag = 0;
    signal(SIGINT, sigint_handler);

	// char audit_file[128];

    unsigned int port = 0;

    while ((opt = getopt(argc, argv, "p:hj:")) != -1) {
        switch (opt) {
            case 'p':
                port = atoi(optarg);
                break;
            case 'h':
                h_flag = 1;
                break;
            case 'j':
                jobs = atoi(optarg);
                break;
            case '?':
                if (optopt == 'j'){
                    fprintf(stdout, "Option j requires argument\n");
                } else {
                    fprintf(stdout, "unknown option %c", optopt);
                }
                exit(EXIT_FAILURE);
            default: 
                fprintf(stderr, ARG_ERROR_MESSAGE, argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    printf("argc = %d optind = %d\n", argc, optind);

	if (argc - optind != 2) {
        // Error case when numer of arguments != 2 (port# & audit file name not given)
        fprintf(stderr, ARG_ERROR_MESSAGE, argv[0]);
        exit(EXIT_FAILURE);
	}

	port = atoi(argv[optind]);
	// strcpy(audit_file, argv[optind + 1]);
    audit_fp = fopen(argv[optind + 1], "w");

    if (port == 0) {
        fprintf(stderr, PORT_ERROR_MESSAGE);
        fprintf(stderr, ARG_ERROR_MESSAGE, argv[0]);
        exit(EXIT_FAILURE);
    }

    // Spawn job threads and init job buffer
    int ct;
    pthread_t tid[jobs];
    sbuf_init(&job_buffer, JOB_BUFFER_SIZE);

    for (ct=0; ct < jobs; ++ct){
        pthread_create(&(tid[ct]), NULL, process_job, NULL);
    }
    
    run_server(port);
    fclose(audit_fp);

    return 0;
}
