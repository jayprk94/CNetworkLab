
#include "room.h"

// Linkedlist operations and wrappers
int indexOfRoom(List_t* list, char* roomName, pthread_mutex_t* mutex, Room** room){
    // if(mutex != NULL){pthread_mutex_lock(mutex);}
    int index = 0;
    node_t* current = list->head;
    while (current != NULL){
        if (strcmp(((Room*)(current->value))->name, roomName) == 0){
            // matching found, return index
            *room = (Room*)(current->value);
            // if(mutex != NULL){pthread_mutex_unlock(mutex);}
            return index;
        }
        current = current->next;
        ++index;
    }
    // if(mutex != NULL){pthread_mutex_unlock(mutex);}
    *room = NULL;
    return -1;
}

void insertRoom(List_t* list, Room* room, pthread_mutex_t* mutex){
    // if(mutex != NULL){pthread_mutex_lock(mutex);}
    insertFront(list, (void*)room);
    // pthread_mutex_unlock(mutex);
    // if(mutex != NULL){pthread_mutex_unlock(mutex);}
}

void removeRoom(List_t* list, char* roomName, pthread_mutex_t* mutex){
    Room* room;
    int index = indexOfRoom(list, roomName, mutex, &room);
    // if(mutex != NULL){pthread_mutex_lock(mutex);}
    removeByIndex(list, index);
    // if(mutex != NULL){pthread_mutex_unlock(mutex);}
}


void roomsToString(char** outstring, List_t* list, pthread_mutex_t* mutex){
    // if(mutex != NULL){pthread_mutex_lock(mutex);}

    char buffer[1024] = "";

    // Iterate rooms 
    node_t* current_r = list->head;
    while (current_r != NULL){
        Room* current_room = (Room*)(current_r->value);
        strcat(buffer, current_room->name);
        strcat(buffer, ": ");

        // Iterate users
        node_t* current_u = current_room->users.head;
        char is_first = 1;
        while (current_u != NULL){
            // Takes care about ","
            if (is_first){is_first = 0;} else {strcat(buffer, ",");}

            User* current_user = (User*)(current_u->value);
            strcat(buffer,current_user->username);
            
            current_u = current_u->next;
        }
        strcat(buffer,"\n");
        current_r = current_r->next;
    }

    *outstring = malloc(sizeof(char) * strlen(buffer) + 1);
    strcpy(*outstring, buffer);
    *(*outstring + strlen(buffer)) = 0;

    // if(mutex != NULL){pthread_mutex_unlock(mutex);}
}