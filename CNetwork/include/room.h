#ifndef ROOM_H
#define ROOM_H
#include "linkedList.h"
#include "user.h"

typedef struct room {
    char* name;
    User* creator;
    List_t users;
} Room;

// Finds room from rooms by room name
int indexOfRoom(List_t* list, char* roomName, pthread_mutex_t* mutex, Room** room);

// Insert room to rooms
void insertRoom(List_t* list, Room* room, pthread_mutex_t* mutex);

// Find and remove room from rooms by room name
void removeRoom(List_t* list, char* roomName, pthread_mutex_t* mutex);

// Returns string representation of room
// roomname:usrname,username,username\n...
void roomsToString(char** outstring, List_t* list, pthread_mutex_t* mutex);
#endif