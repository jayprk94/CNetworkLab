#ifndef SERVICE_H
#define SERVICE_H

#include "user.h"
#include "room.h"
#include "protocol.h"
#include "auditLog.h"

// Service module is capable of sending message to client, modifying Users, Rooms.
// Functions detatched from Server module for code readability and reusability.

// Create room
int logout(List_t* users, List_t* rooms, User* user, pthread_mutex_t *rooms_mutex, pthread_mutex_t *users_mutex);
int rmcreate(List_t* rooms, User* user, char* roomName, pthread_mutex_t* rooms_mutex);
int rmdelete(List_t* rooms, User* user, char* roomName, pthread_mutex_t* rooms_mutex);
void rmlist(List_t* rooms, User* user, pthread_mutex_t* rooms_mutex);
int rmjoin(List_t* rooms, User* user, char* roomName, pthread_mutex_t* rooms_mutex);
int rmleave(List_t* rooms, User* user, char* roomName, pthread_mutex_t* rooms_mutex);
int rmsend(List_t* rooms, User* user, char* string_chunk, pthread_mutex_t* rooms_mutex);
int usrsend(List_t* users, User* user, char* string_chunk, pthread_mutex_t* users_mutex);
int usrlist(List_t* users, User* user, pthread_mutex_t* users_mutex);
void parse_string_chunk(char* string_chunk, char** arg1, char** arg2);

#endif