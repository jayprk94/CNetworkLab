#ifndef USER_H
#define USER_H

#include "linkedList.h"
#include <pthread.h>

typedef struct User {
	int socket_fd;
	char *username;
} User;

// Finds index of user from users by username
int indexOfUser(List_t* list, char* username, User** user);

// Insert user to users
void insertUser(List_t* list, User* user, pthread_mutex_t* mutex);

// Remove user from users by username
void removeUser(List_t* list, char* username);

// String representation of User list
void usersToString(char** outstring, List_t* list, pthread_mutex_t* mutex, char *doNotInclude);
#endif