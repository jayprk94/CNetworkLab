#ifndef HELPERS_H
#define HELPERS_H
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "protocol.h"

void print_header(FILE* outfile, petr_header* header);
void write_buffer(char* buffer, char* body);

#endif
