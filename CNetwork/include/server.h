#ifndef SERVER_H
#define SERVER_H

#include <arpa/inet.h>
#include <getopt.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFFER_SIZE 1024
#define SA struct sockaddr

void run_server(int server_port);

// custom code
#include "helpers.h"
#include "linkedList.h"
#include "sbuf.h"
#include "user.h"
#include "service.h"
#include "auditLog.h"
#define JOB_BUFFER_SIZE 16


#define ARG_ERROR_MESSAGE "Server Application Usage: %s -p <port_number>\n"
#define PORT_ERROR_MESSAGE "ERROR: Port number for server to listen is not given\n"
#define MUTEX_INIT_ERROR_MESSAGE "ERROR: Mutex init failed\n"

#endif
